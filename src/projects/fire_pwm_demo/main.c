 /******  需要调节参数  ******/
#define MOTOR_VALUE_L 50    //使小车直行的左电机占空比
#define MOTOR_VALUE_R 49    //使小车直行的右电机占空比
int P=3;                    // PID的P值，减小P值过弯灵敏

/******  图像采集初始化  ******/
u8  IMG_BUFF[CAMERA_H][CAMERA_W]; //图像缓存区
u8  IMG_finish=0;                 //图像采集完成标记
u8  IMG_get=0;                    //开始采集图像标记
int  H_count=1;                   //当前采集行数   
u8  Cmd;


/******  寻线算法初始化  ******/
#define  ROW_NUM   8    //检测的有效行数
#define  ROW_BEGIN 160 
u16 left_edge[ROW_NUM]={0};  //存储左线位置数组
u16 right_edge[ROW_NUM]={319};  //存储右线位置数组
//int deta=0;     //deta初始化，用于调节电机占空比
int gate=150;  //定义阈值
int angle=135;    //舵机占空比初始化

/******  液晶屏显示初始化  ******/
//u8  lcd_img_buff[8][128];  //液晶屏数据缓存区
#define  LCD_BEGIN 80      //液晶屏显示数据的起始行
u16 mid;                   //由寻线算法计算出的赛道中心

/******  按键初始化  ******/
//u8 D7;         //存储按键端口的状态
//u8 D8;
//u8 D9;
//u8 D10;
//u8 D11;
//u8 mode=0;      //当前程序运行模式

/******  函数声明  ******/
void image_proc(void);      //寻线+计算中线+电机差速

/*void LCD_IMG_BUFF(void);    //处理液晶屏显示缓存（320×160压缩为128×64）
void LCD_IMG(void);         //LCD显示图像
void LCD_IMG_BUFF1(void);   //第4行：实际处理图像；第5行：处理结果
void LCD_IMG1(void);        //LCD显示4行，5行图像*/



#define GPIO_PIN_MASK            0x1Fu
#define GPIO_PIN(x)              (((1)<<(x & GPIO_PIN_MASK)))
void init_gpio()
{

  //Set PTA10, PTA11, PTA28, and PTA29 (connected to LED's) for GPIO functionality
  PORTA_PCR17=(0|PORT_PCR_MUX(1));

  //Change PTA10, PTA11, PTA28, PTA29 to outputs
  GPIOA_PDDR=GPIO_PDDR_PDD(GPIO_PIN(17) );	
}
/******  主函数  ******/

//PC1是INH,PC2正转,PC3反转
//舵机范围0.5-2.5（50-250），可用范围60-120度（117-183）
#define MOTO_R_MOST     183-15
#define MOTO_L_MOST     117-15
#define MOTO_MID        150-15

void main()
{
    init_gpio();
    GPIOA_PDOR&=~GPIO_PDOR_PDO(GPIO_PIN(17));    
    //SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL);
    DisableInterrupts;  //关中断
    FTM_PWM_init(FTM0, CH1, 8000, 200);  //PWM通道初始化
    gpio_init(PORTC,1,GPO,1); //MOTOR4,INH 恒定为1
    gpio_init(PORTC,3,GPO,0);
    FTM_PWM_init(FTM1,CH1,100,MOTO_MID);  //舵机PWM通道（也可能是FTM1的CH0）
    
    delayms(1500);       //延时1.5s，等待电流稳定
    OV7620_init ();     //摄像头初始化
    sccb_init();        //SCCB初始化（修改摄像头配置）
   
    //uart_init(UART1, 19200);    //串口初始化(19200可能需要更改？9600？)
        
    u16 i;

    for(i=0;i<20;i++)
        {
            sccb_regWrite(0x42,0x11,0x01);  //对PCLK二分频
        }
    for(i=0;i<20;i++)
        {
            sccb_regWrite(0x42,0x14,0x20);  //分辨率改为320×240，连续采集
        }
    
    EnableInterrupts;   //开中断
    
    while(1)
    {
        if(IMG_finish==1)  
        {   
            
            disable_irq(90);    //关中断，图像处理过程禁止图像采集
            /*SEGGER_RTT_printf(0, "&&&&&&&&&");
            for(int h = 0 ; h < CAMERA_H ; h++){
              for(int w = 0 ; w < CAMERA_W ; w++){
                SEGGER_RTT_printf(0, " %d ", IMG_BUFF[h][w]);
              }
            }
            SEGGER_RTT_printf(0, "##########");
            while(1);*/
            GPIOA_PDOR&=~GPIO_PDOR_PDO(GPIO_PIN(17));     
            image_proc();  
            GPIOA_PDOR|=GPIO_PDOR_PDO(GPIO_PIN(17)); 
            FTM_PWM_Duty(FTM1, CH1, angle);
            IMG_finish = 0;
            enable_irq(90);     //开中断，继续采集数据    
        }
   }
}

//寻线+赛道识别+转角设置
void image_proc(void)
{
  
   int countleft=0;
   int countright=0;
   for(int i=50;i>40;i--)   //逐行扫描
   {
     if(IMG_BUFF[i][160]<gate)  //小车前方中线上是否有黑色点，若有则遇到弯道，flag置1
     {
       break;
     }
     for(int j=160;j>0;j--)    //从中线开始往左数
     {
       if(IMG_BUFF[i][j]>gate)   
         countleft++;            //中线往左存在灰度值大于阈值的白色点，则计数
     }
     for(int j=160;j<320;j++)     //从中线开始往右数
     {
       if(IMG_BUFF[i][j]>gate)
         countright++;         //中线往右存在灰度值大于阈值的白色点，则计数
     }
   }
    if(countleft-countright>50) //左边白点超过一定数量，小车向左拐小弯
   {
     angle=MOTO_MID+(MOTO_L_MOST-MOTO_MID)/3;     //设置舵机通道占空比，对应偏小角度
     //v=10;
   }
   else if(countright<50||countleft-countright>200)   //左边白点在某个范围之间，小车向左拐大弯
   {
     angle=MOTO_L_MOST;
     //v=10;
   }                                  //还可根据赛道测试的实际情况增加小车转角程度的档位
   else if(countright-countleft>50)
   {
     angle=MOTO_MID+(MOTO_R_MOST-MOTO_MID)/3;
     //v=10;       
   }
   else if(countleft<50||countright-countleft>200)
   {
     angle=MOTO_R_MOST;
     //v=10;       
   }
   else
   {
      angle=MOTO_MID;
   }
}

